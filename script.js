// Map function
const numbers = [1, 2, 3, 4, 5];
console.log('Before map method' +' '+ numbers);

let final = numbers.map(function(val) {
    return val*2;
});
console.log(final);

let final1 = numbers.map(x => x * 5);
console.log(final1);


const employee = [
    { id:1, firstName: "Jagan", lastName: "Raj" },
    { id:2, firstName: "Jagadesh", lastName: "Ravi" },
    { id:3, firstName: "Suriya", lastName: "KSK" },
    { id:4, firstName: "Abilash", lastName: "Coder" },
];

let result = employee.map(function(val) {
    return val.firstName + val.lastName;
});

console.log(result);

let result1 = employee.map(function(val) {
    return [val.firstName, val.lastName].join(" ")
});

console.log(result1);

let result2 = employee.map(function(val) {
    let fullName =  [val.firstName, val.lastName].join(" ")
    let obj = { fullName };
    return obj;
});

console.log(result2);


//Filter function
const age = [16, 18, 20, 25, 30];

let adults = age.filter(function (val){
    return val > 18;
});

console.log("Allowed to rollercoaster:" , adults);


const products = [
    { id:101,  items: 'Redmi', cost: "15000"},
    { id:102,  items: 'iPhone', cost: "65000"},
    { id:103,  items: 'OnePlus', cost: "35000"},
    { id:104,  items: 'samsung', cost: "45000"},
    { id:105,  items: 'sony', cost: "55000"},
    { id:106,  items: 'LG', cost: "5000"},
];

let filteredPhone =  products.filter(function(val){
    return val.cost < 20000;
});

let filteredPhone1 =  products.filter((val) => val.cost > 35000);

console.log("Needed Phone:", filteredPhone);
console.log("Needed Phone:", filteredPhone1);


// Find function
 let phoneCost = products.find((val) => val.cost > 25000);
 console.log("FindPhone:", phoneCost);

// indexOf function
let indexOfValue = age.indexOf((18));
console.log("IndexOf value:", indexOfValue);

// lastIndexOf function
let lastIndexOfValue = age.lastIndexOf((18));
console.log("lastIndexOf value:", lastIndexOfValue);

// Fill function
console.log("fill value:", age.fill(0, 1, 4));

//Some function
const values = (val) => val < 18;
console.log("some values", age.some(values));

//Every function
console.log("every value:", age.every(values));


